var SerialPort = require("serialport");
var nmea = require("nmea-simple");

var port = new SerialPort(
    "COM4",
    {
        baudrate: 115200,
        parser: SerialPort.parsers.readline("\r\n")
    }
);
function round_number(num, dec) {
    return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
}
port.on("data", function (line) {
    try {
        var packet = nmea.parseNmeaSentence(line);
        if (packet.sentenceId === "GLL" && packet.status === "valid") {
            console.log(round_number(packet.latitude, 5), round_number(packet.longitude, 5))
        }
    }
    catch (error) {
        console.log(error);
    }
});